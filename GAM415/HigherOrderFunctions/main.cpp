#include <vector>
#include <iostream>
#include <functional>
#include <stdexcept>
#include <string>
#include <sstream>
using namespace std;




int FindIf(vector<int> v, function<bool(int)> f)
{
	throw runtime_error("FindIf: No matching value found.");
}

function<bool(int)> Negate(function<bool(int)> f)
{
	return [f](int n) -> bool
	{
		return !f(n);
	};
}

auto Double(int x) -> int
{
	return x * 2;
}

// Needs <string> <sstream> <vector>
vector<string> Split(string s, char delimiter)
{
	vector<string> result;

	stringstream ss(s);

	string part;
	while (getline(ss, part, delimiter))
	{
		result.push_back(part);
	}

	return result;
}


int main(int argc, char* argv[])
{
	vector<int> v = {5, 6, 8, 9};

	try
	{
		FindIf(v, [](int n) {return n == 3; });
	}
	catch(exception& e)
	{
		cout << e.what() << endl;
	}

	int localA = 6;

	function<bool(int)> not6 = Negate([localA](int n) {return n == localA; });


	cout << "This is not 6: " << not6(72) << endl;
	cout << "This is 6: " << not6(6) << endl;


	cin.get();

	return 0;
}