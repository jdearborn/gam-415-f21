﻿using System;

class Program
{
    public delegate void ClickHandler();

    public event ClickHandler OnClick;

    public void PrintClick()
    {
        Console.WriteLine("PrintClick");
    }

    /*
     * Is a Singleton problematic?
     * 1) There's only ever one
     * 2) Can be accessed ANYWHERE
     */

    /*
       In Unity:
       
       Create a Singleton CharacterManager that has an event OnCharacterDeath.

       Create a Character script (MonoBehaviour) that
         destroys its game object after a random time within a few seconds.
           * Use OnDestroy to invoke the CharacterManager's OnCharacterDeath event.
           
       Create a UI text element with an attached script that subscribes to 
         the OnCharacterDeath event (use +=) and keeps count of the number of 
         characters that have died.  Be sure to deregister (use -=) when the text's
         script is destroyed.  This will ensure that if this observer is ever
         destroyed, then the event won't call back into a dead object.

       CharacterManager spawns Character objects randomly over time.

     */

    static void Main(string[] args)
    {
        Console.WriteLine("Hello!");

        Program p = new Program();

        //ClickHandler c = new ClickHandler();
        //c += () => { };

        p.OnClick += () => { Console.WriteLine("Click1"); };
        p.OnClick += p.PrintClick;

        p.OnClick.Invoke();

        Console.ReadLine();

    }
}