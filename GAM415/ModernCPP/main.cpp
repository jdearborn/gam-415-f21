#include <iostream>
#include <vector>
#include <list>
#include <set>
#include <map>
#include <string>
#include <algorithm>
#include <memory>
using namespace std;


template<typename T>
class MyClass
{
public:

	T n;
};

int main(int argc, char* argv[])
{
	auto n = 0;
	auto l = 0L;
	auto g = 0.0f;
	auto d = 0.0;

	vector<int> v;
	v.push_back(50);

	// v.size() : size_t

	char c = 0;
	unsigned char f = 50;
	signed char b = -127;

	size_t t;
	for (auto i = 0u; i < v.size(); ++i)
	{
		cout << v[i] << endl;
	}


	
	list<MyClass<vector<int>>> ls;
	ls.push_back(MyClass<vector<int>>());

	for (auto e = ls.begin(); e != ls.end(); ++e)
	{
		cout << e->n.size() << endl;
	}


	set<int> mySet;
	mySet.insert(4);
	mySet.insert(2);
	mySet.insert(4);
	mySet.insert(2);
	mySet.insert(4);
	mySet.insert(2);
	mySet.insert(4);
	mySet.insert(2);
	mySet.insert(4);
	mySet.insert(2);

	cout << "MySet: " << endl;
	for (auto e = mySet.begin(); e != mySet.end(); ++e)
	{
		cout << *e << endl;
	}


	map<string, int> myMap;

	// myMap.Add("thing", 567);
	myMap.insert(make_pair("thing", 567));

	// range-based for
	for (auto p : myMap)
	{
		cout << p.first << ", " << p.second << endl;
	}

	int array[] = {204, 342, 234};
	for (auto num : array)
	{
		cout << num << endl;
	}

	for (auto& myObj : ls)
	{
		cout << myObj.n.size() << endl;
	}



	//std::find_first_of(v.begin(), v.end(),)
	for_each(v.begin(), v.end(), [](int i) {
		cout << i << endl;
	});

	vector <shared_ptr<MyClass<int>>> objects;

	{
		MyClass<int>* myPtr = new MyClass<int>();
		shared_ptr<MyClass<int>> myOtherPtr = make_shared<MyClass<int>>();
		// RAII
		// Resource Acquisition Is Initialization
		// "Rule of Three"

		cout << myOtherPtr->n << endl;

		auto yetAnother = myOtherPtr;

		objects.push_back(yetAnother);


		delete myPtr;
	}

	// Still alive!
	cout << objects[0]->n << endl;

	// Now dead.
	objects.clear();


	return 0;
}