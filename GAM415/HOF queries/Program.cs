﻿using System;
using System.IO;
using System.Collections.Generic;

/*
 * HOF application
		Given a database (.csv) of player IDs and (random) info…
			Plan how you will deserialize this data.
			Generate a list of players who are over level 7
			Calculate the average playtime of all players who have beaten stage 6
			Calculate the amount of gold given out if we gave each player under level 3 some bonus gold equal to of 25% of their current XP.
 */

namespace HOF_queries
{
    class Program
    {
        static T FindIf<T>(List<T> values, Func<T, bool> fn)
        {
            for(int i = 0; i < values.Count; ++i)
            {
                if (fn(values[i]))
                    return values[i];
            }
            throw new Exception();
        }

        static List<T> Filter<T>(List<T> values, Func<T, bool> fn)
        {
            List<T> result = new();
            for(int i = 0; i < values.Count; ++i)
            {
                if (fn(values[i]))
                    result.Add(values[i]);
            }
            return result;
        }

        static List<U> Map<T, U>(List<T> values, Func<T, U> fn)
        {
            List<U> result = new();
            for(int i = 0; i < values.Count; ++i)
            {
                result.Add(fn(values[i]));
            }
            return result;
        }

        static U ReduceLeft<T, U>(List<T> values, U initial, Func<U, T, U> fn)
        {
            U result = initial;
            for(int i = 0; i < values.Count; ++i)
            {
                result = fn(result, values[i]);
            }
            return result;
        }

        static void Main(string[] args)
        {
            StreamReader r = new("player data.csv");

            List<Player> players = new();

            r.ReadLine();  // Skip the headers

            while (!r.EndOfStream)
            {
                string line = r.ReadLine();

                players.Add(new(line));
            }

            // Generate a list of players who are over level 7
            List<Player> playersOver17 = Filter(players, (p) => { return p.level > 17; });
            Console.WriteLine("Num Players over 17: " + playersOver17.Count);

            //Calculate the average playtime of all players who have beaten stage 6
            List<Player> playersBeat6 = Filter(players, (p) => { return p.progress > 6; });
            //List<int> playTimesBeat6 = Map(playersBeat6, (p) => { return p.playTime; });
            int totalPlay = ReduceLeft(playersBeat6, 0, (a, b) => { return a+b.playTime;});
            float averageTimeBeat6 = totalPlay / (float)playersBeat6.Count;
            Console.WriteLine("Average time beaten stage 6: " + averageTimeBeat6);


            // Calculate the amount of gold given out if we gave each player under level 3
            // some bonus gold equal to of 25% of their current XP.
            List<Player> playersUnder3 = Filter(players, (p) => { return p.level < 3; });
            float goldGiven = ReduceLeft(playersUnder3, 0f, (a, b) => { return a + 0.25f*b.xp; });
            Console.WriteLine("Gold bonus: " + goldGiven);



            Console.ReadLine();
        }
    }
}
