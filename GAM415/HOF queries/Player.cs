﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HOF_queries
{
    struct Player
    {
        public int ID;
        public int playTime;
        public int gold;
        public int xp;
        public int charType;
        public int level;
        public int progress;
        public bool banned;

        public Player(string line)
        {
            string[] s = line.Split(',');
            ID = int.Parse(s[0]);
            playTime = int.Parse(s[1]);
            gold = int.Parse(s[2]);
            xp = int.Parse(s[3]);
            charType = int.Parse(s[4]);
            level = int.Parse(s[5]);
            progress = int.Parse(s[6]);
            banned = (s[7] == "Banned");
        }
    }
}
