#pragma once

#if defined(BUILD_HOF_LIBRARY)
	#define EXPORT_HOF_LIBRARY __declspec(dllexport)
#else
	#define EXPORT_HOF_LIBRARY __declspec(dllimport)
#endif


EXPORT_HOF_LIBRARY void UsefulFunction();

inline int GetHeaderVersion()
{
	return 2;
}

EXPORT_HOF_LIBRARY int GetLibraryVersion();
