#include <iostream>
#include <functional>
#include <vector>
#include <algorithm>
using namespace std;

typedef void (*ClickHandler)();

// Old C is DUMB
void PrintThing(void)
{
	printf("Hello!\n");
}

int AddStuff(int *a, int b)
{
	return *a + b;
}

void LinePointThing(int x, int y)
{
	printf("Yay, (%d,%d)\n", x, y);
}

void DrawBresenhamLine(int x1, int y1, int x2, int y2, function<void(int,int)> callback)
{
	float rise = y2 - y1;
	float run = x2 - x1;
	float slope = rise / run;

	if (abs(slope) < 1) {
		int y = 0;
		for (int i = fmin(x1, x2); i < fmax(x1, x2); i++) {
			y = (int)((i - fmin(x1, x2)) * slope); //step*slope
			if (slope > 0)
				callback(i, y + fmin(y1, y2));
			if (slope < 0)
				callback(i, y + fmax(y1, y2));
		}
	}
	if (abs(slope) == 1) {
		if (slope > 0) { //slope is positive
			for (int i = 0; i < (fmax(x1, x2) - fmin(x1, x2)); i++) {
				callback(i + fmin(x1, x2), i + fmin(y1, y2));
			}
		}
		if (slope < 0) { //slope is negative
			for (int i = 0; i < (fmax(x1, x2) - fmin(x1, x2)); i++) {
				callback(i + fmin(x1, x2), fmax(y1, y2) - i);
			}
		}
	}
	if (abs(slope) > 1) {
		slope = run / rise;
		int x = 0;
		for (int i = fmin(y1, y2); i < fmax(y1, y2); i++) {
			x = (int)((i - fmin(y1, y2)) * slope);
			if (slope > 0) //slope +
				callback(x + fmin(x1, x2), i);
			if (slope < 0) //slope -
				callback(x + fmax(x1, x2), i);
		}
	}
}

int main(int argc, char* argv[])
{
	// Callback: A function that can be stored and called later
	PrintThing();


	ClickHandler c;

	int a;
	int* pInTheA = &a;
	*pInTheA = 8;

	int *b = &a;

	// Function pointer
	std::function<void()> myFn;
	myFn = &PrintThing;

	function<int(int*, int)> myAddFn = AddStuff;

	myFn();
	myAddFn(b, a);

	myFn = PrintThing;  // Much nicer!
	myFn();

	myAddFn(b, a);

	DrawBresenhamLine(3, 4, 8, 10, LinePointThing);



	auto myXYPrint = [&](int x, int y)
	{
		a++;
		cout << x << ", " << y << endl;
	};
	// C#: (thing) => {stuff;}

	DrawBresenhamLine(3, 4, 8, 10, myXYPrint);

	cout << "a is now: " << a << endl;

	void* ptr;
	ptr = &a;

	cout << *(int*)ptr << endl;


	vector<int> myVector;

	myVector.push_back(3);

	auto e = std::find(myVector.begin(), myVector.end(), 3);
	cout << "Found 3: " << *e << endl;

	e = std::find_if(myVector.begin(), myVector.end(), [](int i) { return i == 3; });




	cin.get();

	return 0;
}
