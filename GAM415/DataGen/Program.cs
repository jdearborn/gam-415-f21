﻿using System;
using System.IO;
/*
 * HOF application
		Given a database (.csv) of player IDs and (random) info…
			Plan how you will deserialize this data.
			Generate a list of players who are over level 7
			Calculate the average playtime of all players who have beaten stage 6
			Calculate the amount of gold given out if we gave each player under level 3 some bonus gold equal to of 25% of their current XP.
 */
namespace DataGen
{
    class Program
    {
        static void Main(string[] args)
        {
            StreamWriter writer = new StreamWriter("data.csv");

            Random rng = new Random();

            writer.WriteLine("Player ID,Play time,Gold,XP,Chartype,Level,Highest stage beaten,Ban status");

            int numPlayers = rng.Next(1200, 2000);
            for (int i = 0; i < numPlayers; ++i)
            {
                int level = rng.Next(1, 101);
                writer.Write(i + ",");
                writer.Write((rng.Next(1, 10) + rng.Next(5, 20)*level*level) + ",");

                bool isRich = rng.Next(0, 101) == 100;
                if(isRich)
                    writer.Write((rng.Next(0, 200) + rng.Next(0, 400) * level * level) + ",");
                else
                    writer.Write((rng.Next(0, 200) + rng.Next(0, 400) * level) + ",");

                writer.Write((rng.Next(0, 100) + rng.Next(0, 1000) + level*level*1200) + ",");
                writer.Write(rng.Next(0, 10) + ",");
                writer.Write(level + ",");

                int stage = level/rng.Next(2,20) + (int)(level/10f * rng.Next(1, 3));
                writer.Write(stage + ",");
                if(rng.Next(0, 101) == 100)
                    writer.Write("Banned,");
                else
                    writer.Write(",");

                writer.WriteLine();
            }

            writer.Close();
        }
    }
}
