#include <iostream>
#include <conio.h>
using namespace std;

int main(int argc, char* argv[])
{
	while(true)
	{
		char c = _getch();  // Doesn't wait for Enter like cin.get()

		cout << c << endl;
	}

	//getch
	//_getch - The POSIX standards-compliant version

	//strcpy
	//strcpy_s - The "safe" version

	return 0;
}