#include <iostream>
#include "SDL_gpu.h"
#include "NFont_gpu.h"
using namespace std;

int main(int argc, char* argv[])
{
	GPU_Target* screen = GPU_Init(800, 600, GPU_DEFAULT_INIT_FLAGS);
	if (screen == nullptr)
		return 1;

	SDL_SetWindowTitle(SDL_GetWindowFromID(screen->context->windowID), "My Awesome Game");
	
	GPU_Image* smileImage = GPU_LoadImage("smile.png");
	if (smileImage == nullptr)
		return 2;

	NFont font;
	font.load("FreeSans.ttf", 14);

	const Uint8* keystates = SDL_GetKeyboardState(nullptr);

	int score = 0;

	int mx = 0, my = 0;
	float startX = 0;
	float startY = 0;
	float endX = 800;
	float endY = 600;

	SDL_Event event;
	bool done = false;
	while (!done)
	{
		while (SDL_PollEvent(&event))
		{
			if (event.type == SDL_QUIT)
				done = true;
			if (event.type == SDL_KEYDOWN)
			{
				if (event.key.keysym.sym == SDLK_ESCAPE)
					done = true;
				if (event.key.keysym.sym == SDLK_SPACE)
					score += 5;
			}
			if (event.type == SDL_MOUSEBUTTONDOWN)
			{
				if (event.button.button == SDL_BUTTON_LEFT)
				{
					startX = event.button.x;
					startY = event.button.y;
				}
				if (event.button.button == SDL_BUTTON_RIGHT)
				{
					endX = event.button.x;
					endY = event.button.y;
				}
			}
		}

		SDL_GetMouseState(&mx, &my);


		GPU_ClearRGB(screen, 100, 255, 100);

		float x = 0.0f;
		float y = 0.0f;

		float t = SDL_GetTicks() / 1000.0f;
		while (t > 1.0f)
		{
			t -= 1.0f;
		}

		/*if (t > 0.5f)
		{
			t = 0.5f;
		}*/
		// Ternary operator    ?:
		//t = (t > 0.5f ? 0.5f : t);

		// Interpolating from a start point to an end point
		//float tx = 0.5f * (3 - 2 * t) * t * t;
		//x = startX * (1 - tx) + endX * tx;
		//float ty = (sin(t) + 1)/2;
		//y = startY * (1 - ty) + endY * ty;

		// Stepping across the screen
		x = t;
		// Use easing function to calculate normalized y coordinate
		y = (sin(t) + 1) / 2;

		// Scale coordinates to screen (pixel) coordinates
		x = 800 * x;
		//Flip y-coordinate
		y = 600 * (1 - y);

		GPU_Blit(smileImage, nullptr, screen, x, y);

		string s = "Hello";
		font.draw(screen, 50, 10, "Score: %d, %s", score, s.c_str());
		font.draw(screen, screen->w - 50, 10, NFont::AlignEnum::RIGHT, "Mouse: (%d, %d)", mx, my);

		GPU_Flip(screen);

		SDL_Delay(1);
	}

	GPU_FreeImage(smileImage);
	font.free();

	GPU_Quit();

	return 0;
}