using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleport : MonoBehaviour
{
    Material mat;

    // Start is called before the first frame update
    void Start()
    {
        mat = GetComponentInChildren<Renderer>().material;
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetButtonDown("Fire1"))
        {
            StartCoroutine(DoTeleport(transform.forward, 2, 2f));
        }
    }

    IEnumerator DoTeleport(Vector3 direction, float distance, float rate)
    {
        float t = 0f;
        while (t < 1f)
        {
            t += Time.deltaTime * rate;
            mat.SetFloat("DissolveThreshold", t);
            yield return null;
        }

        transform.position += direction * distance;

        while(t > 0f)
        {
            t -= Time.deltaTime * rate;
            mat.SetFloat("DissolveThreshold", t);
            yield return null;
        }
    }
}
