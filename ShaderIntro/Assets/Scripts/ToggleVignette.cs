using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleVignette : MonoBehaviour
{
    public Material mat;
    private bool toggle = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            if (toggle)
                mat.SetFloat("Intensity", 0.5f);
            else
                mat.SetFloat("Intensity", 2.0f);
            toggle = !toggle;
        }
    }
}
