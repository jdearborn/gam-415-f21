Shader "Unlit/UnlitTestShader"
{
    Properties
    {
        _MainTex ("Main texture", 2D) = "white" {}
        Red ("Red value", Float) = 1.0
        Blue ("Blue value", Range(0,1)) = 1.0
        PrimaryColor("Primary color", Color) = (1, 1, 1)
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;

            float Red;
            float Blue;
            float3 PrimaryColor;

            v2f vert (appdata v)
            {
                v2f o;
                //o.vertex = UnityObjectToClipPos(v.vertex * (1 + sin(Blue * _Time[1])) / 2);
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            float4 frag(v2f i) : SV_Target
            {
                // sample the texture
                float4 col = tex2D(_MainTex, i.uv + float2(_Time[1], _Time[1]*0.75));
                //col *= Blue;
                //col = float4(step(i.uv.x, 0.5), col.gba);
                 
                //float4 col = float4(PrimaryColor.rgb, 1.0);
                
                // Branches are technically legal...
                //if (col.r < 0.75)
                //    col.r = 0;
                
                // Prefer "branchless" programming in shaders
                //col.r = col.r * step(col.r, 0.75);

                // apply fog
                UNITY_APPLY_FOG(i.fogCoord, col);
                return col;
            }
            ENDCG
        }
    }
}
