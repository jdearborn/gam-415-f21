Shader "Custom/LUTShader"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("Albedo (RGB)", 2D) = "white" {}
        LUT ("Look Up Table (RGB)", 2D) = "white" {}
        LUTWidth ("Number of LUT columns", int) = 8
        LUTHeight ("Number of LUT rows", int) = 8
        _Glossiness ("Smoothness", Range(0,1)) = 0.5
        _Metallic ("Metallic", Range(0,1)) = 0.0
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200

        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Standard fullforwardshadows

        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 3.0

        sampler2D _MainTex;
        sampler2D LUT;
        int LUTWidth;
        int LUTHeight;

        struct Input
        {
            float2 uv_MainTex;
        };

        half _Glossiness;
        half _Metallic;
        fixed4 _Color;

        // Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
        // See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
        // #pragma instancing_options assumeuniformscaling
        UNITY_INSTANCING_BUFFER_START(Props)
            // put more per-instance properties here
        UNITY_INSTANCING_BUFFER_END(Props)

        void surf (Input IN, inout SurfaceOutputStandard o)
        {
            // Albedo comes from a texture tinted by color
            float4 c1 = tex2D (_MainTex, IN.uv_MainTex) * _Color;

            float blueCellIndex = c1.z * (LUTWidth * LUTHeight);

            // Break down the blue channel into row and column in a WxH-cell grid
            int row = floor(blueCellIndex) / LUTWidth;
            int column = floor(blueCellIndex) % LUTWidth;

            // Locate the exact texel based on row,column offsets and adding the red and green channels
            float2 coords = float2((column + c1.x)/float(LUTWidth), 1-(row + c1.y)/float(LUTHeight));

            // Perform the lookup
            float4 c = tex2D (LUT, coords);


            o.Albedo = c.rgb;
            // Metallic and smoothness come from slider variables
            o.Metallic = _Metallic;
            o.Smoothness = _Glossiness;
            o.Alpha = c.a;
        }
        ENDCG
    }
    FallBack "Diffuse"
}
